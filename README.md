# stretch4x_code

Related to the master's thesis "Heterogeneous hw / sw acceleration of a video game in a reconfigurable MPSoC" carried out at the UPM University in 2018. This is a Vivado HLS project source code to create the hardware implementation of the "stretch2x" function from the Crispy-DOOM source code. Its functionality is to read an input 640x400 frame and create an output 1280x960 frame.  

## Getting Started

You have to include this files in a Vivado project or Vivado SDSoC.

### src_hls/
Code to use with Vivado HLS.

### src_sdsoc/
Code to generate the bistream with the desirable number of modules.

### src_speedup/
Example of files to generate only the .elf. 

### scripts/
Scripts to generate the code to insert in **stretc4x_tb.cpp** files in sdsoc and speedup, and to plot the results.
INA226 script also included.


## Authors

* **David Lima** - *davidlimaastor@gmail.com* - [BitBucket](https://bitbucket.org/d_lima)

## License

No license, feel free to use as an example.

