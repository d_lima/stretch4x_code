/* ----------------------------------------------------------------------------
 * ------------------------- Doom TFM (CEI 2017/2018)--------------------------
 * ----------------------------------------------------------------------------
 * 	Stretch4x Accelerator
 * 		File: - "stretch4x_hw.cpp"
 *
 * ----------------------------------------------------------------------------
 * Author:  David Lima (davidlimaastor@gmail.com)
 *
 *
 * TODO:
 *
 * CHANGELOG:
 *
 *---------------------------------------------------------------------------*/
#include "stretch4x_hw.h"
#include "stdio.h"

/*---------------------------------------------------------------------------*/
/*-
* Hardware module to be synthesized. This functionality resides on copy each
* pixels values 4 times (RGB -> RRR RGG GGB BBB). This will increase the with
* of the image to 1280 pixels. In the case of height, each line is copied 4.8
* times, so for each 5 lines in the input image, 24 will be written in the
* output (line 0 to 3 will be written 5 times and last line will be written 4.)
*
* @param	x1, x2, y1, y2 are static arguments currently not used in the hw
* 			implementation of the function.
* @param    src and dest are the AXIStreams lines to receive and send
* 			data with the DMA.
*
* @note		4x stretch (1280x960)
*
*----------------------------------------------------------------------------*/
void Stretch4x(int hSizeInputSlice, int wSizeInputSlice, byte *srcFrame, byte *videoFrame)
{
#pragma HLS INTERFACE axis register both port=videoFrame
#pragma HLS INTERFACE axis register both port=srcFrame

    int y, x, z;                       // Loop variables
    byte temp_value; 				   // To store the temporal value
    byte line_buffer[320]; // To store all the line

    /* For every 5 lines of srcFrame, 24 lines are written to video */
    // (200 -> 960)
    for_height:for (y=0; y<(hSizeInputSlice); y += 5)
    {
        /* Store the data in line 0 */
        src_line0:for (x=0; x<wSizeInputSlice; x++)
        {
#pragma HLS UNROLL factor=2
            line_buffer[x] = *srcFrame;
            srcFrame++;
        }

        /* Write 5 times the line */
        line_0: for (z=0; z<5; z++)
	    {
	        write_line0:for (x=0; x<wSizeInputSlice; x++)
	        {
	        	temp_value = line_buffer[x];
				*videoFrame = temp_value;
				videoFrame++;
				*videoFrame = temp_value;
				videoFrame++;
				*videoFrame = temp_value;
				videoFrame++;
				*videoFrame = temp_value;
				videoFrame++;
	        }
	    } // line_0


        /* Store the data in line 1 */
        srcFrame_line1:for (x=0; x<wSizeInputSlice; x++)
        {
#pragma HLS UNROLL factor=2
        	line_buffer[x] = *srcFrame;
            srcFrame++;
        }

        /* Write 5 times line 1 */
        line_1:for (z=0; z<5; z++)
        {
        	write_line1:for (x=0; x<wSizeInputSlice; x++)
	        {
            	temp_value = line_buffer[x];
				*videoFrame = temp_value;
				videoFrame++;
				*videoFrame = temp_value;
				videoFrame++;
				*videoFrame = temp_value;
				videoFrame++;
				*videoFrame = temp_value;
				videoFrame++;
	        }
        } // line 1


        /* Store the data in line 2 */
        srcFrame_line2:for (x=0; x<wSizeInputSlice; x++)
	    {
#pragma HLS UNROLL factor=2
        	line_buffer[x] = *srcFrame;
            srcFrame++;
	    }

        /* Write 5 times line 2 */
	    line_2:for (z=0; z<5; z++)
	    {
	        write_line2:for (x=0; x<wSizeInputSlice; x++)
	        {
            	temp_value = line_buffer[x];
				*videoFrame = temp_value;
				videoFrame++;
				*videoFrame = temp_value;
				videoFrame++;
				*videoFrame = temp_value;
				videoFrame++;
				*videoFrame = temp_value;
				videoFrame++;
	        }
	    } // line 2


        /* Store the data in line 3 */
	    srcFrame_line3:for (x=0; x<wSizeInputSlice; x++)
	    {
#pragma HLS UNROLL factor=2
	    	line_buffer[x] = *srcFrame;
            srcFrame++;
	    }

        /* Write 5 times line 3 */
	    line_3:for (z=0; z<5; z++)
	    {
	        write_line3:for (x=0; x<wSizeInputSlice; x++)
            {
            	temp_value = line_buffer[x];
				*videoFrame = temp_value;
				videoFrame++;
				*videoFrame = temp_value;
				videoFrame++;
				*videoFrame = temp_value;
				videoFrame++;
				*videoFrame = temp_value;
				videoFrame++;
	        }
	    } // line_3


        /* Store the data in line 4 */
	    srcFrame_line4:for (x=0; x<wSizeInputSlice; x++)
	    {
#pragma HLS UNROLL factor=2
	    	line_buffer[x] = *srcFrame;
            srcFrame++;
	    } // store_line_4

        /* Write 4 times line 4 */
        line_4:for (z=0; z<4; z++)
	    {
        	write_line4:for (x=0; x<wSizeInputSlice; x++)
	        {
            	temp_value = line_buffer[x];
				*videoFrame = temp_value;
				videoFrame++;
				*videoFrame = temp_value;
				videoFrame++;
				*videoFrame = temp_value;
				videoFrame++;
				*videoFrame = temp_value;
				videoFrame++;
	        }

         } // line_4

    } // loop_height

}
