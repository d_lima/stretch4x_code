/* ----------------------------------------------------------------------------
 * ------------------------- Doom TFM (CEI 2017/2018)--------------------------
 * ----------------------------------------------------------------------------
 * 	Stretch4x Accelerator project
 * 		File: - "stretch4x_tb.h"
 *
 * ----------------------------------------------------------------------------
 * Author:  David Lima (davidlimaastor@gmail.com)
 *
 *
 * TODO:
 *
 * CHANGELOG:
 *
 *---------------------------------------------------------------------------*/
#ifndef STRETCH4X_TB_H
#define STRETCH4X_TB_H


/*----------------------------- Libraries -----------------------------------*/
#include <hls_video.h>
#include <hls_opencv.h>
#include <stddef.h>
#include <stdio.h>
#include <iostream>
#include <sys/time.h>
#include <time.h>

/*---------------------------- Project headers ------------------------------*/
#include "stretch4x_hw.h"
#include "stretch4x_sw.h"
#include "doomtype.h"

/*------------------------- Constant Definitions ----------------------------*/
/*
 * Software static arguments, needed in software implementation
 */
#define X1 (0)
#define X2 (320)
#define Y1 (0)
#define Y2 (200)

/*
 * Pitch of destination buffer, ie. screen->pitch
 */
#define PITCH (1280)

/*------------------------- Variable Definitions ----------------------------*/
/*
 * Global pointers needed in SW implementation
 */
byte *src_buffer, *dest_buffer;
int dest_pitch;


/*------------------------- Function Prototypes -----------------------------*/
byte* matToBytes(cv::Mat image);
cv::Mat bytesToMat(byte * bytes,int width,int height);
void saveInput(cv::Mat img);


#endif // STRETCH4X_TB_H
