/* ----------------------------------------------------------------------------
 * ------------------------- Doom TFM (CEI 2017/2018)--------------------------
 * ----------------------------------------------------------------------------
 * 	Stretch4x Accelerator project
 * 		File: - "stretch4x_tb.cpp"
 *
 * ----------------------------------------------------------------------------
 * Author:  David Lima (davidlimaastor@gmail.com)
 *
 *
 * TODO:
 *
 * CHANGELOG:
 *     [12-11-2018]: Hardware module changed to work with arrays of structures.
 *
 *---------------------------------------------------------------------------*/
#include "stretch4x_tb.h"

using namespace std;


/*---------------------------------------------------------------------------*/
/*-
* Convert cv::Mat data to *byte
*
* @param	cv::Mat with image values.
*
* @return   byte pointer to data in u8(unsigned char) format.
*
* @note		None
*
*----------------------------------------------------------------------------*/
byte* matToBytes(cv::Mat image)
{
    int size = image.total() * image.channels();
    byte * bytes = new byte[size];

    std::memcpy(bytes,image.data,size * sizeof(byte));

    return bytes;
}


/*---------------------------------------------------------------------------*/
/*-
* Convert *byte to cv::Mat
*
* @param	byte pointer to data in u8.
* @param    Width of the desirable cv::Mat output.
* @param 	Height of the desirable cv::Mar output.
*
* @return   Data in cv::Mat.
*
* @note		None
*
*----------------------------------------------------------------------------*/
cv::Mat bytesToMat(byte* bytes,int width,int height)
{
     cv::Mat image = cv::Mat(height, width, CV_8UC3, bytes).clone();

     return image;
}


/*---------------------------------------------------------------------------*/
/*-
* Convert *struct_data to cv::Mat

*
* @param	struct_data pointer to data.
* @param    Width of the desirable cv::Mat output.
* @param 	Height of the desirable cv::Mar output.
*
* @return   Data in cv::Mat.
*
* @note		None
*
*----------------------------------------------------------------------------*/
/*
cv::Mat structToMat(struct_data *struct_data, int width, int height)
{
	byte *bytes;
	bytes = (byte*)malloc(width * height * 3 * sizeof(byte));

	for (int i=0; i<(width*height); i++)
	{
		bytes[i] = struct_data->data;
		struct_data++;
	}

    cv::Mat image = cv::Mat(height, width, CV_8UC3, bytes).clone();

    return image;
}
*/

/*---------------------------------------------------------------------------*/
/*-
* Save the values of input in a .txt file
*
* @param	cv::Mat image to be store on disk
*
* @return   None
*
* @note		This is needed to generate the file used in bareMetal test.
*
*----------------------------------------------------------------------------*/
void saveInput(cv::Mat img)
{
    byte *input_data;
    FILE *save_input;

    unsigned char file_data[320*200*3];

    input_data = matToBytes(img);

    save_input = fopen("input.txt", "w");

    for (int h=0; h<(200*320*3); h++)
    {
	    file_data[h] = *input_data;
	    input_data++;
    }

    fwrite(file_data, 1, sizeof(file_data), save_input);
    fclose(save_input);
}


/*------------------------------- Main --------------------------------------*/
int main(int argc, char** argv )
{
    /* Errors counter */
    int c_errors = 0;

    /* Pointers to images*/
    byte *check_golden;		// Check golden copy
    byte *check_out_sw;		// check software output
    byte *check_out_hw; 	// check hardware output

    byte *bytes_img_in;

    /* Time measurements */
    struct timeval t0, tf;
    float t_hw, t_sw;

    /* Read the input and golden images */
    cv::Mat img_in = cv::imread("doom.PPM",cv::IMREAD_COLOR);
    saveInput(img_in);
    cv::Mat img_golden = cv::imread("doom_golden.PPM", cv::IMREAD_COLOR);

    /* 1280x960x3 (RGB) output images */
    cv::Mat img_out_hw = cv::Mat(SCREENHEIGHT*4.8, SCREENWIDTH*4, CV_8UC3);
    cv::Mat img_out_sw = cv::Mat(SCREENHEIGHT*4.8, SCREENWIDTH*4, CV_8UC3);

    /* Structures used in the HW */
    byte *input;
    byte *output;

    /* Print some info about the input image */
    printf("----------------------\n");
    printf("Input image name: doom.PPM\n");
    printf("Number of channels: %d\n",img_in.channels());
    printf("Rows: %d\n",img_in.rows);
    printf("Columns: %d\n",img_in.cols);
    printf("Pixel size: %u\n",img_in.elemSize());

    input = (byte *)malloc(64000 * sizeof(byte));
    if (input == NULL) { fputs("Memory error input", stderr); exit(2); }
    output = (byte *)malloc(1228800 * sizeof(byte));
    if (output == NULL) { fputs("Memory error output", stderr); exit(2); }

    /*----------------------------------
    ------------- Hardware -------------
    ----------------------------------*/
    printf("----------------------\n");
    printf("Executing Hardware implementation ...\n");

    input = matToBytes(img_in);

    printf("Calling the UUT ...");
    gettimeofday(&t0, NULL);
    Stretch4x(200, 320, input, output);
    gettimeofday(&tf, NULL);
    printf("OK\n");

    t_hw = ((tf.tv_sec - t0.tv_sec) * 1000.0) + ((tf.tv_usec - t0.tv_usec) / 1000.0);
    printf("Hardware execution: %.6f\n", t_hw);

    /* Store result */
    printf("Writing image to Doom_out_hw.PPM ...");
    img_out_hw = bytesToMat(output, 1280, 960);
    cv::imwrite("Doom_out_hw.PPM", img_out_hw);
    printf("OK\n");


    /*----------------------------------
    ------------- Software -------------
    ----------------------------------*/
    printf("----------------------\n");
    printf("Executing Software implementation ...\n");

    /* This is needed to move the destination pointer */
    dest_pitch = PITCH;

    /* Destination has 4 times rows and 4.8 times columns */
    dest_buffer = (byte*)malloc(SCREENWIDTH*4 * SCREENHEIGHT*4.8 \
      * img_in.channels() * sizeof(byte));

    /* Transform input in bytes */
    src_buffer = matToBytes(img_in);

    /* Call software */
    printf("Calling SW implementation ...");
    gettimeofday(&t0, NULL);
    I_Stretch4x_SW(X1, Y1, X2, Y2);
    gettimeofday(&tf, NULL);
    printf("OK\n");

    t_sw = ((tf.tv_sec - t0.tv_sec) * 1000.0) + ((tf.tv_usec - t0.tv_usec) / 1000.0);
    printf("Software execution: %.6f\n", t_sw);

    /* Transform into cv::Mat to store the result */
    img_out_sw = bytesToMat(dest_buffer, SCREENWIDTH*4, SCREENHEIGHT*4.8);

    printf("Writing image to Doom_out_sw.PPM ...");
    cv::imwrite("Doom_out_sw.PPM", img_out_sw);
    printf("OK\n");


    /*-------------------------------------
    --------------- Check -----------------
    -------------------------------------*/
    printf("----------------------\n");
    printf("Checking results ...\n");

    /* Take pointers to check values */
    printf("Gathering images information ...");
    check_golden = matToBytes(img_golden);
    check_out_hw = matToBytes(img_out_hw);
    check_out_sw = matToBytes(img_out_sw);
    printf("OK\n");

    /* Loop to check bytes */
    printf("Checking bytes in both images ... \n");

    for (int i=0; i<(1280*960); i++)
    {
        /* Check if values are different */
        if ((*check_golden != *check_out_sw) | \
        		(*check_golden != *check_out_hw)) {

            c_errors++;
            printf("i: %u\t golden: %u, HW: %u, SW: %u \n",i , *check_golden,
                 *check_out_hw, *check_out_sw);

        } // end if differences

        /* Next values */
        check_golden++;
        check_out_hw++;
        check_out_sw++;

    } // for I values
    printf("...\n");
    printf("OK\n");

    /* Free memory */
    free(dest_buffer);
    free(input);
    free(output);

    printf("Checking simulation result... \n");
    if (c_errors!=0)
    {
        fprintf(stdout, "-------------------------------------------\n");
        fprintf(stdout, "FAIL: Output DOES NOT match the golden output\n");
        fprintf(stdout, "Number of errors: %d \n", c_errors);
        fprintf(stdout, "-------------------------------------------\n");
        return 1;
    } // end if no errors
    else
    {
        fprintf(stdout, "-------------------------------------------\n");
        fprintf(stdout, "PASS: The output matches the golden output!\n");
	    fprintf(stdout, "-------------------------------------------\n");
	    return 0;
    } // end if errors

}
