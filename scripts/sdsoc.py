#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 13 11:55:45 2019

_p0_Stretch4x1_async_1(25, 320, bytes_img_in + 0*320*sizeof(byte), bytes_img_out + 0*1280*sizeof(byte));


@author: limiloiko
"""

# input
fragments = 1

step_input = 200/fragments
step_output = 960/fragments

#print(step_input)

print("")
print("With STUB functions, use with already generated bitstream:")
print("")

for i in range(1, fragments+1, 1):
    print("#pragma SDS async(%u)"%(i))
    print("#pragma SDS resource(%u)"%(i))
    print("_p0_Stretch4x%u_async_%u(%u, 320, bytes_img_in + %u*320*sizeof(byte), bytes_img_out + %u*1280*sizeof(byte));"%(i,i,step_input,(i-1)*step_input, (i-1)*step_output))
    
    
for i in range(1, fragments+1, 1):
    print("sds_wait(%u);"%i)          
    print("#pragma SDS wait(%u)"%i)
          

print("")
print("With SDSoC to generate bistream ")
print("")
          
for i in range(1, fragments+1, 1):
    print("#pragma SDS async(%u)"%(i))
    print("#pragma SDS resource(%u)"%(i))
    print("Stretch4x(%u, 320, bytes_img_in + %u*320*sizeof(byte), bytes_img_out + %u*1280*sizeof(byte));"%(step_input,(i-1)*step_input, (i-1)*step_output))
    
    
for i in range(1, fragments+1, 1):
    print("#pragma SDS wait(%u)"%i)