#!/bin/sh

# This script loads each bitstream and prints the sensors information 
# related to the PL consumption.

# INA226 info for addresses here:
# https://www.xilinx.com/support/documentation/boards_and_kits/zcu102/ug1182-zcu102-eval-bd.pdf

# PL:
# sensors ina226-i2c-4-40| grep power1

# PS: !!!!!!! NOT CHECKED !!!!!!!
# sensors ina226-i2c-5-40| grep power1

# Execute for 100 MHZ
printf "Load/printing for 100 MHz setups \n"
cd 100MHz/1_module
printf "1 module: "
cp stretch2x_1module_100mhz.bin /lib/firmware/
echo stretch2x_1module_100mhz.bin > /sys/class/fpga_manager/fpga0/firmware
sensors ina226-i2c-4-40| grep power1
sleep 2

cd ../2_module
printf "2 module: "
cp stretch2x_2modules_100mhz.bin /lib/firmware/
echo stretch2x_2modules_100mhz.bin > /sys/class/fpga_manager/fpga0/firmware
sensors ina226-i2c-4-40 | grep power1
sleep 2

cd ../4_module
printf "4 module: "
cp stretch2x_4modules_100mhz.bin /lib/firmware/
echo stretch2x_4modules_100mhz.bin > /sys/class/fpga_manager/fpga0/firmware
sensors ina226-i2c-4-40 | grep power1
sleep 2

cd ../5_module
printf "5 module: "
cp stretch2x_5modules_100mhz.bin /lib/firmware/
echo stretch2x_5modules_100mhz.bin > /sys/class/fpga_manager/fpga0/firmware
sensors ina226-i2c-4-40 | grep power1
sleep 2

cd ../8_module
printf "8 module: "
cp stretch2x_8modules_100mhz.bin /lib/firmware/
echo stretch2x_8modules_100mhz.bin > /sys/class/fpga_manager/fpga0/firmware
sensors ina226-i2c-4-40 | grep power1
sleep 2 

echo '--------------------------------'

# Execute for 150 MHZ
printf "Load/printing for 150 MHz setups \n"
cd ../../150MHz/1_module
printf "1 module: "
cp stretch2x_1module_150mhz.bin /lib/firmware/
echo stretch2x_1module_150mhz.bin > /sys/class/fpga_manager/fpga0/firmware
sensors ina226-i2c-4-40| grep power1
sleep 2 

cd ../2_module
printf "2 module: "
cp stretch2x_2modules_150mhz.bin /lib/firmware/
echo stretch2x_2modules_150mhz.bin > /sys/class/fpga_manager/fpga0/firmware
sensors ina226-i2c-4-40 | grep power1
sleep 2

cd ../4_module
printf "4 module: "
cp stretch2x_4modules_150mhz.bin /lib/firmware/
echo stretch2x_4modules_150mhz.bin > /sys/class/fpga_manager/fpga0/firmware
sensors ina226-i2c-4-40 | grep power1
sleep 2

cd ../5_module
printf "5 module: "
cp stretch2x_5modules_150mhz.bin /lib/firmware/
echo stretch2x_5modules_150mhz.bin > /sys/class/fpga_manager/fpga0/firmware
sensors ina226-i2c-4-40 | grep power1
sleep 2

cd ../8_module
printf "8 module: "
cp stretch2x_8modules_150mhz.bin /lib/firmware/
echo stretch2x_8modules_150mhz.bin > /sys/class/fpga_manager/fpga0/firmware
sensors ina226-i2c-4-40 | grep power1
sleep 2

echo '--------------------------------'

# Execute for 200 MHZ
printf "Load/printing for 200 MHz setups \n"
cd ../../200MHz/1_module
printf "1 module: "
cp stretch2x_1module_200mhz.bin /lib/firmware/
echo stretch2x_1module_200mhz.bin > /sys/class/fpga_manager/fpga0/firmware
sensors ina226-i2c-4-40| grep power1
sleep 2

cd ../2_module 
printf "2 module: "
cp stretch2x_2modules_200mhz.bin /lib/firmware/
echo stretch2x_2modules_200mhz.bin > /sys/class/fpga_manager/fpga0/firmware
sensors ina226-i2c-4-40 | grep power1
sleep 2

cd ../4_module
printf "4 module: "
cp stretch2x_4modules_200mhz.bin /lib/firmware/
echo stretch2x_4modules_200mhz.bin > /sys/class/fpga_manager/fpga0/firmware
sensors ina226-i2c-4-40 | grep power1
sleep 2

cd ../5_module
printf "5 module: "
cp stretch2x_5modules_200mhz.bin /lib/firmware/
echo stretch2x_5modules_200mhz.bin > /sys/class/fpga_manager/fpga0/firmware
sensors ina226-i2c-4-40 | grep power1
sleep 2

cd ../8_module
printf "8 module: "
cp stretch2x_8modules_200mhz.bin /lib/firmware/
echo stretch2x_8modules_200mhz.bin > /sys/class/fpga_manager/fpga0/firmware
sensors ina226-i2c-4-40 | grep power1
sleep 2

echo '--------------------------------'

# Execute for 300 MHZ
printf "Load/printing for 300 MHz setups \n"
cd ../../300MHz/1_module
printf "1 module: "
cp stretch2x_1module_300mhz.bin /lib/firmware/
echo stretch2x_1module_300mhz.bin > /sys/class/fpga_manager/fpga0/firmware
sensors ina226-i2c-4-40| grep power1
sleep 2

cd ../2_module
printf "2 module: "
cp stretch2x_2modules_300mhz.bin /lib/firmware/
echo stretch2x_2modules_300mhz.bin > /sys/class/fpga_manager/fpga0/firmware
sensors ina226-i2c-4-40 | grep power1
sleep 2

cd ../4_module
printf "4 module: "
cp stretch2x_4modules_300mhz.bin /lib/firmware/
echo stretch2x_4modules_300mhz.bin > /sys/class/fpga_manager/fpga0/firmware
sensors ina226-i2c-4-40 | grep power1
sleep 2

cd ../5_module
printf "5 module: "
cp stretch2x_5modules_300mhz.bin /lib/firmware/
echo stretch2x_5modules_300mhz.bin > /sys/class/fpga_manager/fpga0/firmware
sensors ina226-i2c-4-40 | grep power1
sleep 2

cd ../8_module
printf "8 module: "
cp stretch2x_8modules_300mhz.bin /lib/firmware/
echo stretch2x_8modules_300mhz.bin > /sys/class/fpga_manager/fpga0/firmware
sensors ina226-i2c-4-40 | grep power1
sleep 2