# -*- coding: utf-8 -*-
"""
Editor de Spyder

Este es un archivo temporal.


"""


from matplotlib.pylab import plt
from matplotlib.pyplot import savefig


"""
    STRETCH4x DATA !
"""

# Speedups
mhz100_speed=[0.67, 1.35, 2.40, 2.59, 3.30]
mhz150_speed=[0.99, 1.87, 3.20, 3.60, 4.13]
mhz200_speed=[1.38, 2.43, 3.96, 4.37, 4.74]
mhz300_speed=[1.37, 2.56, 4.11, 4.53, 4.83]

# PL consumption
mhz100_plpower=[0.375, 0.400, 0.500, 0.525, 0.650]
mhz150_plpower=[0.400, 0.500, 0.575, 0.775, 0.850]
mhz200_plpower=[0.475, 0.650, 0.725, 0.900, 1.020]
mhz300_plpower=[0.500, 0.700, 0.975, 1.150, 1.430]

# Efficiency: speedupt/plconsumption
mhz100_eff=[ x/y for x, y in zip(mhz100_speed, mhz100_plpower)]
mhz150_eff=[ x/y for x, y in zip(mhz150_speed, mhz150_plpower)]
mhz200_eff=[ x/y for x, y in zip(mhz200_speed, mhz200_plpower)]
mhz300_eff=[ x/y for x, y in zip(mhz300_speed, mhz300_plpower)]

# FPS in GAME
fps_100=[55,28,39,46,49,54]
fps_150=[55,33,44,51,53,55]
fps_200=[55,38,47,53,55,56]
fps_300=[55,39,49,53,56,56]


"""
    STRETCH"X DATA
"""


## Speedups
#mhz100_speed=[]
#mhz150_speed=[]
#mhz200_speed=[]
#mhz300_speed=[]
#
## PL consumption
#mhz100_plpower=[]
#mhz150_plpower=[]
#mhz200_plpower=[]
#mhz300_plpower=[]
#
## Efficiency: speedupt/plconsumption
#mhz100_eff=[ x/y for x, y in zip(mhz100_speed, mhz100_plpower)]
#mhz150_eff=[ x/y for x, y in zip(mhz150_speed, mhz150_plpower)]
#mhz200_eff=[ x/y for x, y in zip(mhz200_speed, mhz200_plpower)]
#mhz300_eff=[ x/y for x, y in zip(mhz300_speed, mhz300_plpower)]
#
## FPS in GAME
#fps_100=[]
#fps_150=[]
#fps_200=[]
#fps_300=[]



"""
     Curvas speedup
"""

def curvas_speedup():

    # DATA:
    
    X_axis = [1,2,4,5,8]
    xi = [i for i in range(0, len(X_axis))]
    
    
    plt.plot(mhz100_speed, label='100 MHz', marker='o')
    plt.plot(mhz150_speed, label='150 MHz', marker='o')
    plt.plot(mhz200_speed, label='200 MHz', marker='o')
    plt.plot(mhz300_speed, label='300 MHz', marker='o')
    
    plt.xlabel('Nº modules')
    plt.ylabel('Speed-up')
    
    plt.legend(loc='upper left')
    
    
    plt.xticks(xi, X_axis)
    plt.grid()
    savefig('speedups.png')

"""
    PS power
"""

def ps_power():
    
    # DATA:
    ps_power=[1.20, 1.07, 1.07, 1.07, 1.07, 1.07]
    
    X_axis = [0, 1,2,4,5,8]
    xi = [i for i in range(0, len(X_axis))]
    
    
    plt.plot(ps_power, marker='o')
    
    plt.xlabel('Nº modules')
    plt.ylabel('Power(W)')
    plt.ylim(0.90, 1.30)
    
    plt.xticks(xi, X_axis)
    plt.grid()
    savefig('pspower.png')

"""
     PL power
"""

def pl_power():

    # DATA:
    
    
    X_axis = [1,2,4,5,8]
    xi = [i for i in range(0, len(X_axis))]
    
    
    plt.plot(mhz100_plpower, label='100 MHz', marker='o')
    plt.plot(mhz150_plpower, label='150 MHz', marker='o')
    plt.plot(mhz200_plpower, label='200 MHz', marker='o')
    plt.plot(mhz300_plpower, label='300 MHz', marker='o')
    
    plt.xlabel('Nº modules')
    plt.ylabel('Power(W)')
    
    plt.legend(loc='upper left')
    
    
    plt.xticks(xi, X_axis)
    plt.grid()
    savefig('plpower.png')



"""
    Efficiency curves
"""

def effiency_curves():

    # DATA:
    
    X_axis = [1,2,4,5,8]
    xi = [i for i in range(0, len(X_axis))]
    
    
    plt.plot(mhz100_eff, label='100 MHz', marker='o')
    plt.plot(mhz150_eff, label='150 MHz', marker='o')
    plt.plot(mhz200_eff, label='200 MHz', marker='o')
    plt.plot(mhz300_eff, label='300 MHz', marker='o')
    
    plt.xlabel('Nº modules')
    plt.ylabel('Efficiency (speed-up/mW)')
    
    plt.legend(loc='upper left')
    
    
    plt.xticks(xi, X_axis)
    plt.grid()
    savefig('efficiencycurves.png')


def fps():
    
    X_axis = [0,1,2,4,5,8]
    xi = [i for i in range(0, len(X_axis))]
    
    
    plt.plot(fps_100, label='100 MHz', marker='o')
    plt.plot(fps_150, label='150 MHz', marker='o')
    plt.plot(fps_200, label='200 MHz', marker='o')
    plt.plot(fps_300, label='300 MHz', marker='o')
    
    plt.xlabel('Nº modules')
    plt.ylabel('FPS Rate')
    
    plt.legend(loc='lower right')
    
    
    plt.xticks(xi, X_axis)
    plt.grid()
    savefig('fps.png')
    

def amdhal(percentaje, single_speedup):
    theoretical = 1.0 / ((1-percentaje)+(percentaje/single_speedup))
    return theoretical

    
def amdhals_law():

    stretch_percentaje = 0.6853
    
    X_axis = [1, 2, 4, 5, 8]
    xi = [i for i in range(0, len(X_axis))]
    
    amdhal_100 = [amdhal(stretch_percentaje, j) for j in mhz100_speed]
    print(amdhal_100)
    amdhal_150 = [amdhal(stretch_percentaje, j) for j in mhz150_speed]
    amdhal_200 = [amdhal(stretch_percentaje, j) for j in mhz200_speed]
    amdhal_300 = [amdhal(stretch_percentaje, j) for j in mhz300_speed]
    
    
    plt.plot(amdhal_100, label='100 MHz', marker='o')
    plt.plot(amdhal_150, label='150 MHz', marker='o')
    plt.plot(amdhal_200, label='200 MHz', marker='o')
    plt.plot(amdhal_300, label='300 MHz', marker='o')
    
    plt.xlabel('Nº modules')
    plt.ylabel('Theoretical Speedup')
    
    plt.legend(loc='lower right')
    
    
    plt.xticks(xi, X_axis)
    plt.grid()
    savefig('amdhal.png')
    
    
    
if __name__=="__main__":
    
#    pl_power()
#    effiency_curves()
    amdhals_law()
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    